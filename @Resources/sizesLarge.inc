[Variables]

; Sizes

TextVMarginRatio=(1/4)
PrimaryTextFontSize=(24 * #SizeMultiplier#)
SecondaryTextFontSize=(12 * #SizeMultiplier#)
SecondarySmallTextFontSize=(10 * #SizeMultiplier#)
BigTextFontSize=(18 * #SizeMultiplier#)
MediumTextFontSize=(12 * #SizeMultiplier#)
SmallTextFontSize=(10 * #SizeMultiplier#)
DefaultBoxWidth=(350 * #SizeMultiplier#)

PrimaryTextHeight=(#PrimaryTextFontSize# * 2)
SecondaryTextHeight=(#SecondaryTextFontSize# * 2)
SecondarySmallTextHeight=(#SecondarySmallTextFontSize# * 2)
BigTextHeight=(#BigTextFontSize# * 2)
MediumTextHeight=(#MediumTextFontSize# * 2)
SmallTextHeight=(#SmallTextFontSize# * 2)