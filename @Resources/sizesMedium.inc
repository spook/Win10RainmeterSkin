[Variables]

; Sizes

TextVMarginRatio=(1/4)
PrimaryTextFontSize=(18 * #SizeMultiplier#)
SecondaryTextFontSize=(10 * #SizeMultiplier#)
SecondarySmallTextFontSize=(9 * #SizeMultiplier#)
BigTextFontSize=(16 * #SizeMultiplier#)
MediumTextFontSize=(10 * #SizeMultiplier#)
SmallTextFontSize=(8 * #SizeMultiplier#)
DefaultBoxWidth=(350 * #SizeMultiplier#)

PrimaryTextHeight=(#PrimaryTextFontSize# * 2)
SecondaryTextHeight=(#SecondaryTextFontSize# * 2)
SecondarySmallTextHeight=(#SecondarySmallTextFontSize# * 2)
BigTextHeight=(#BigTextFontSize# * 2)
MediumTextHeight=(#MediumTextFontSize# * 2)
SmallTextHeight=(#SmallTextFontSize# * 2)