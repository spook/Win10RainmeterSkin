[StylePrimaryText]
H=#PrimaryTextHeight#
AntiAlias=1
FontFace=Segoe UI Light
FontSize=#PrimaryTextFontSize#
FontColor=#EmphasizeColor#
StringAlign=Left
ClipString=0

[StylePrimaryAccentText]
H=#PrimaryTextHeight#
AntiAlias=1
FontFace=Segoe UI Light
FontSize=#PrimaryTextFontSize#
FontColor=#AccentColor#
StringAlign=Left
ClipString=0

[StyleSecondaryText]
H=#SecondaryTextHeight#
AntiAlias=1
FontFace=Segoe UI Light
FontSize=#SecondaryTextFontSize#
FontColor=#AccentColor#
StringAlign=Left
ClipString=0

[StyleSecondarySmallText]
H=#SecondarySmallTextHeight#
AntiAlias=1
FontFace=Segoe UI Light
FontSize=#SecondarySmallTextFontSize#
FontColor=#AccentColor#
StringAlign=Left
ClipString=0

[StyleBigText]
H=#BigTextHeight#
AntiAlias=1
FontFace=Segoe UI Light
FontSize=#BigTextFontSize#
FontColor=#EmphasizeColor#
StringAlign=Left
ClipString=0

[StyleMediumText]
H=#MediumTextHeight#
AntiAlias=1
FontFace=Segoe UI Light
FontSize=#MediumTextFontSize#
FontColor=#EmphasizeColor#
StringAlign=Left
ClipString=0

[StyleSmallText]
H=#SmallTextHeight#
AntiAlias=1
FontFace=Segoe UI Light
FontSize=#SmallTextFontSize#
FontColor=#EmphasizeColor#
StringAlign=Left
ClipString=0

[StyleMuted]
FontColor=#MutedColor#