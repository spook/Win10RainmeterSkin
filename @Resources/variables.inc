[Variables]

; DPI fix
SizeMultiplier=1.0

; Colors - dark
; EmphasizeColor=255,255,255,255
; AccentColor=80,160,240,255
; MutedColor=94,94,94,255
; BackgroundColor=20,20,20,230
; BorderColor=180,180,180,128

; Colors - bright
EmphasizeColor=0,0,0
AccentColor=20,128,235,255
MutedColor=94,94,94,255
BackgroundColor=209,227,238,230
BorderColor=180,180,180,128

; Constants

Kilobyte=(1024)
Megabyte=(1024*1024)
Gigabyte=(1024*1024*1024)