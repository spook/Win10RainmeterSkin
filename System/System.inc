; Measures

[MeasureCpu]
Measure=Cpu

[MeasureCPUFreq]
Measure=Plugin
Plugin=PowerPlugin
PowerState=MHz

[MeasureMemory]
Measure=PhysicalMemory

[MeasureTotalMemory]
Measure=PhysicalMemory
Total=1

[MeasureMemoryPercent]
Measure=Calc
Formula=(100*(MeasureMemory/MeasureTotalMemory))

[MeasureNetIn]
Measure=NetIn

[MeasureNetOut]
Measure=NetOut

; Meters

[MeterBackground]
Meter=Shape
Shape=Rectangle 0,0,#DefaultBoxWidth#,#BoxHeight# | StrokeWidth 1 | Stroke Color #BorderColor# | Fill Color #BackgroundColor#

; Meters - CPU

[CpuHeaderMeter]
Meter=String
MeterStyle=#MeterHeaderStyle#
Text=CPU
X=#CpuHeaderX#
Y=#CpuHeaderY#

[CPUGHzMeter]
Meter=String
MeasureName=MeasureCPUFreq
MeterStyle=#MeterValueStyle#
X=0R
Y=#CpuHeaderY#
Scale=1000
NumOfDecimals=1
Text=@ %1 GHz

[CpuValueMeter]
Meter=String
MeterStyle=#MeterValueStyle#
StringAlign=RightTop
MeasureName=MeasureCpu
Text=%1%
X=#CpuValueX#
Y=#CpuValueY#

[CpuHistogramFrame]
Meter=Shape
X=#CpuFrameX#
Y=#CpuFrameY#
Shape=Rectangle 0,0,#CpuFrameW#,#CpuFrameH# | StrokeWidth 1 | Stroke Color #CpuFrameColor# | Fill Color #CpuBackgroundColor#

[CpuHistogramMeter]
Meter=Histogram
MeasureName=MeasureCpu
PrimaryColor=#CpuHistogramColor#
X=#CpuHistogramX#
Y=#CpuHistogramY#
W=#CpuHistogramW#
H=#CpuHistogramH#

[CpuLineMeter]
Meter=Line
MeasureName=MeasureCpu
LineColor=#CpuLineColor#
X=#CpuLineX#
Y=#CpuLineY#
W=#CpuLineW#
H=#CpuLineH#

; Meters - memory

[MemoryHeaderMeter]
Meter=String
MeterStyle=#MeterHeaderStyle#
Text=Memory
X=#MemoryHeaderX#
Y=#MemoryHeaderY#

[MemoryValueMeter]
Meter=String
MeterStyle=#MeterValueStyle#
StringAlign=RightTop
MeasureName=MeasureMemoryPercent
Text=%1%
X=#MemoryValueX#
Y=#MemoryValueY#

[MemoryHistogramFrame]
Meter=Shape
X=#MemoryFrameX#
Y=#MemoryFrameY#
Shape=Rectangle 0,0,#MemoryFrameW#,#MemoryFrameH# | StrokeWidth 1 | Stroke Color #MemoryFrameColor# | Fill Color #MemoryBackgroundColor#

[MemoryHistogramMeter]
Meter=Histogram
MeasureName=MeasureMemory
PrimaryColor=#MemoryHistogramColor#
X=#MemoryHistogramX#
Y=#MemoryHistogramY#
W=#MemoryHistogramW#
H=#MemoryHistogramH#

[MemoryLineMeter]
Meter=Line
MeasureName=MeasureMemory
LineColor=#MemoryLineColor#
X=#MemoryLineX#
Y=#MemoryLineY#
W=#MemoryLineW#
H=#MemoryLineH#

; Meters - network

[NetHeaderInMeter]
Meter=String
MeterStyle=#MeterHeaderStyle#
Text=Network in
X=#NetworkInHeaderX#
Y=#NetworkInHeaderY#

[NetValueMeterIn]
Meter=String
MeterStyle=#MeterValueStyle#
StringAlign=RightTop
MeasureName=MeasureNetIn
AutoScale=1k
Text=%1Bps
X=#NetworkInValueX#
Y=#NetworkInValueY#

[NetInHistogramFrame]
Meter=Shape
X=#NetworkInFrameX#
Y=#NetworkInFrameY#
Shape=Rectangle 0,0,#NetworkInFrameW#,#NetworkInFrameH# | StrokeWidth 1 | Stroke Color #NetInFrameColor# | Fill Color #NetBackgroundColor#

[NetInHistogramMeter]
Meter=Histogram
MeasureName=MeasureNetIn
PrimaryColor=#NetInHistogramColor#
AutoScale=1
X=#NetworkInHistogramX#
Y=#NetworkInHistogramY#
W=#NetworkInHistogramW#
H=#NetworkInHistogramH#

[NetInLineMeter]
Meter=Line
MeasureName=MeasureNetIn
LineColor=#NetInLineColor#
AutoScale=1
X=#NetworkInLineX#
Y=#NetworkInLineY#
W=#NetworkInLineW#
H=#NetworkInLineH#

; Network out

[NetHeaderOutMeter]
Meter=String
MeterStyle=#MeterHeaderStyle#
Text=Network out
X=#NetworkOutHeaderX#
Y=#NetworkOutHeaderY#

[NetValueMeterOut]
Meter=String
MeterStyle=#MeterValueStyle#
StringAlign=RightTop
MeasureName=MeasureNetOut
AutoScale=1k
Text=%1Bps
X=#NetworkOutValueX#
Y=#NetworkOutValueY#

[NetOutHistogramFrame]
Meter=Shape
X=#NetworkOutFrameX#
Y=#NetworkOutFrameY#
Shape=Rectangle 0,0,#NetworkOutFrameW#,#NetworkOutFrameH# | StrokeWidth 1 | Stroke Color #NetOutFrameColor# | Fill Color #NetBackgroundColor#

[NetOutHistogramMeter]
Meter=Histogram
MeasureName=MeasureNetOut
PrimaryColor=#NetOutHistogramColor#
AutoScale=1
X=#NetworkOutHistogramX#
Y=#NetworkOutHistogramY#
W=#NetworkOutHistogramW#
H=#NetworkOutHistogramH#

[NetOutLineMeter]
Meter=Line
MeasureName=MeasureNetOut
LineColor=#NetOutLineColor#
AutoScale=1
X=#NetworkOutLineX#
Y=#NetworkOutLineY#
W=#NetworkOutLineW#
H=#NetworkOutLineH#