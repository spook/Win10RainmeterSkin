[MeasureTime]
Measure=Time
Format=%H:%M:%S

[MeasureDate]
Measure=Time
Format=%A, %d-%m-%Y

[MeasureUptime]
Measure=Uptime
Format=%4!i!d %3!i!:%2!02i!

[MeterBackground]
Meter=Shape
Shape=Rectangle 0,0,#DefaultBoxWidth#,#BoxHeight# | StrokeWidth 1 | Stroke Color #BorderColor# | Fill Color #BackgroundColor#

[MeterTime]
Meter=String
MeterStyle=#TimeStyle#
MeasureName=MeasureTime
X=#TimeX#
Y=#TimeY#

[MeterDate]
Meter=String
MeterStyle=#DateStyle#
MeasureName=MeasureDate
X=#DateX#
Y=#DateY#

[MeterUptime]
Meter=String
MeterStyle=#UptimeStyle#
MeasureName=MeasureUptime
Text=Up: %1
StringAlign=Right
X=#UptimeX#
Y=#UptimeY#