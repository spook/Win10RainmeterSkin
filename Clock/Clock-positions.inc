[Variables]
; Positions
TimeX=#LeftPadding#
TimeY=#TopPadding#
TimeStyle=StylePrimaryText
DateX=(#LeftPadding# + (5 * #SizeMultiplier#))
DateY=(-#PrimaryTextHeight# / 5)R
DateStyle=StyleSecondaryText
UptimeStyle=StyleMediumText | StyleMuted
UptimeX=(#DefaultBoxWidth#-#LeftPadding#)
UptimeY=0r