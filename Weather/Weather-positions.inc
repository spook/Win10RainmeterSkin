[Variables]
; Positions
TemperatureX=#LeftPadding#
TemperatureY=#TopPadding#
TemperatureStyle=StyleBigText
PressureY=(#BoxHeight#*(1/3)-#MediumTextHeight#/2)
PressureX=(160 * #SizeMultiplier#)
PressureStyle=StyleMediumText | StyleMuted
HumidityY=(#BoxHeight#*(2/3)-#MediumTextHeight#/2)
HumidityX=(160 * #SizeMultiplier#)
HumidityStyle=StyleMediumText | StyleMuted