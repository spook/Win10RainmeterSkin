[MeasureWeather]
Measure=WebParser
UpdateRate=5
URL=https://api.openweathermap.org/data/3.0/onecall?lat=51.10&lon=17.03&units=metric&appid=#ApiKey#
RegExp=(?siU)^(.*)$
OnUpdateAction=[!CommandMeasure MeasureWeatherUpdater "Execute 1"]

[MeasureWeatherUpdater]
Measure=Plugin
Plugin=ActionTimer
ActionList1=Wait 5000 | SetSource | UpdateTemperature | UpdateHumidity | UpdatePressure | UpdateIcon | RedrawAll
SetSource=[!SetOption "MeasureJson" "Source" [MeasureWeatherJsonCapture]]
UpdateTemperature=[!SetOption "MeterTemperature" "Text" [MeasureJson:Query("current.temp")]][!UpdateMeter "MeterTemperature"]
UpdateHumidity=[!SetOption "MeterHumidity" "Text" [MeasureJson:Query("current.humidity")]][!UpdateMeter "MeterHumidity"]
UpdatePressure=[!SetOption "MeterPressure" "Text" [MeasureJson:Query("current.pressure")]][!UpdateMeter "MeterPressure"]
UpdateIcon=[!Log [MeasureJson:Query("current.weather(0).icon")]][!SetOption "MeterIcon" "ImageName" [MeasureJson:Query("current.weather(0).icon")]][!UpdateMeter "MeterIcon"]
RedrawAll=[!Redraw]

[MeasureWeatherJsonCapture]
Measure=WebParser
URL=[MeasureWeather]
StringIndex=1

[MeasureJson]
Measure=Plugin
Plugin=PluginJsonParser
Precision=1
Source=
Query=
Debug=0

[MeterBackground]
Meter=Shape
Shape=Rectangle 0,0,#DefaultBoxWidth#,#BoxHeight# | StrokeWidth 1 | Stroke Color #BorderColor# | Fill Color #BackgroundColor#

[MeterIcon]
Meter=Image
ImagePath=#@#Weather\
ImageName=01d
X=#IconX#
Y=#IconY#
W=#IconW#
H=#IconH#

[MeterTemperature]
Meter=String
MeterStyle=#TemperatureStyle#
MeasureName=MeasureTemperature
X=5R
Y=#TemperatureY#

[MeterDeg]
Meter=String
MeterStyle=#TemperatureStyle#
Text=o
X=-8R
Y=(-#PrimaryTextFontSize#*(2/5))r

[MeterCelcius]
Meter=String
MeterStyle=#TemperatureStyle#
Text=C
X=-8R
Y=(#PrimaryTextFontSize#*(2/5))r

[MeterHumidityText]
Meter=String
MeterStyle=#HumidityStyle#
Text=Humidity: 
X=#HumidityX#
Y=#HumidityY#

[MeterHumidity]
Meter=String
MeterStyle=#HumidityStyle#
X=0R
Y=0r

[MeterHumidityPercent]
Meter=String
MeterStyle=#HumidityStyle#
Text=%  
X=0R
Y=0r

[MeterPressureText]
Meter=String
MeterStyle=#PressureStyle#
Text=Pressure: 
X=#PressureX#
Y=#PressureY#

[MeterPressure]
Meter=String
MeterStyle=#PressureStyle#
X=0R
Y=0r

[MeterPressureHpa]
Meter=String
MeterStyle=#PressureStyle#
Text=hpa 
X=0R
Y=0r
