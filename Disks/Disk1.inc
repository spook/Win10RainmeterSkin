; Measures - Disk 1

[MeasureTotalDisk1]
Measure=FreeDiskSpace
Drive=#Disk1#
Total=1

[MeasureTotalDisk1Display]
Measure=Calc
Formula=MeasureTotalDisk1 / #Gigabyte#

[MeasureFreeDisk1]
Measure=FreeDiskSpace
Drive=#Disk1#

[MeasureFreeDisk1Display]
Measure=Calc
Formula=MeasureFreeDisk1 / #Gigabyte#

[MeasureUsedDisk1]
Measure=FreeDiskSpace
Drive=#Disk1#
InvertMeasure=1

[MeasureUsedDisk1Display]
Measure=Calc
Formula=MeasureUsedDisk1 / #Gigabyte#

; Meters - Disk 1

[MeterBackground]
Meter=Shape
Shape=Rectangle 0,0,#DefaultBoxWidth#,#BoxHeight# | StrokeWidth 1 | Stroke Color #BorderColor# | Fill Color #BackgroundColor#

[MeterDisk1]
Meter=String
MeterStyle=#DiskStyle#
StringAlign=LeftBottom
Text=#Disk1#
X=#HorizontalPadding#r
Y=#FirstDiskY#

[MeterDiskFree1]
Meter=String
MeterStyle=#DiskFreeStyle#
StringAlign=LeftBottom
MeasureName=MeasureFreeDisk1Display
NumOfDecimals=1
X=#DiskFreeX#
Y=#DiskFreeY#

[MeterDiskFreeGb1]
Meter=String
MeterStyle=#DiskFreeGbStyle#
StringAlign=LeftBottom
Text=GB
X=#DiskFreeGbX#
Y=#DiskFreeGbY#

[MeterDiskUsed1]
Meter=String
MeterStyle=#DiskUsedStyle#
StringAlign=RightBottom
MeasureName=MeasureUsedDisk1Display
NumOfDecimals=1
Text=%1GB Used
X=#DiskUsedX#
Y=#DiskUsedY#

[MeterFreeBar1]
Meter=Bar
MeasureName=MeasureUsedDisk1
BarOrientation=Horizontal
BarColor=#AccentColor#
SolidColor=#MutedColor#
X=#FreeBarX#
Y=#FreeBarY#
W=#FreeBarW#
H=#FreeBarH#