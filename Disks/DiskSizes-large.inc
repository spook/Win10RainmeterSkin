[Variables]
; Sizes
TopPadding=10
HorizontalPadding=20

FirstDiskY=(#TopPadding# + #BigTextHeight#)
DiskStyle=StyleBigText

DiskFreeX=(1.2*#BigTextHeight#)r
DiskFreeY=0r
DiskFreeStyle=StyleBigText

DiskFreeGbX=-5R
DiskFreeGbY=0r
DiskFreeGbStyle=StyleBigText

DiskUsedX=(#DefaultBoxWidth#-#HorizontalPadding#)
DiskUsedY=0r
DiskUsedStyle=StyleMediumText | StyleMuted

FreeBarW=#DefaultBoxWidth#
FreeBarH=6
FreeBarX=0
FreeBarY=(#TopPadding#-#FreeBarH#)r

NextDiskY=(#TopPadding# + #BigTextHeight#)R
