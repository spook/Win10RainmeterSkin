; Measures - Disk 3

[MeasureTotalDisk3]
Measure=FreeDiskSpace
Drive=#Disk3#
Total=1

[MeasureTotalDisk3Display]
Measure=Calc
Formula=MeasureTotalDisk3 / #Gigabyte#

[MeasureFreeDisk3]
Measure=FreeDiskSpace
Drive=#Disk3#

[MeasureFreeDisk3Display]
Measure=Calc
Formula=MeasureFreeDisk3 / #Gigabyte#

[MeasureUsedDisk3]
Measure=FreeDiskSpace
Drive=#Disk3#
InvertMeasure=1

[MeasureUsedDisk3Display]
Measure=Calc
Formula=MeasureUsedDisk3 / #Gigabyte#

; Meters - Disk 3

[MeterBackground]
Meter=Shape
Shape=Rectangle 0,0,#DefaultBoxWidth#,#BoxHeight# | StrokeWidth 1 | Stroke Color #BorderColor# | Fill Color #BackgroundColor#

[MeterDisk3]
Meter=String
MeterStyle=#DiskStyle#
StringAlign=LeftBottom
Text=#Disk3#
X=#HorizontalPadding#r
Y=#NextDiskY#

[MeterDiskFree3]
Meter=String
MeterStyle=#DiskFreeStyle#
StringAlign=LeftBottom
MeasureName=MeasureFreeDisk3Display
NumOfDecimals=1
X=#DiskFreeX#
Y=#DiskFreeY#

[MeterDiskFreeGb3]
Meter=String
MeterStyle=#DiskFreeGbStyle#
StringAlign=LeftBottom
Text=GB
X=#DiskFreeGbX#
Y=#DiskFreeGbY#

[MeterDiskUsed3]
Meter=String
MeterStyle=#DiskUsedStyle#
StringAlign=RightBottom
MeasureName=MeasureUsedDisk3Display
NumOfDecimals=1
Text=%1GB Used
X=#DiskUsedX#
Y=#DiskUsedY#

[MeterFreeBar3]
Meter=Bar
MeasureName=MeasureUsedDisk3
BarOrientation=Horizontal
BarColor=#AccentColor#
SolidColor=#MutedColor#
X=#FreeBarX#
Y=#FreeBarY#
W=#FreeBarW#
H=#FreeBarH#