; Measures - Disk 4

[MeasureTotalDisk4]
Measure=FreeDiskSpace
Drive=#Disk4#
Total=1

[MeasureTotalDisk4Display]
Measure=Calc
Formula=MeasureTotalDisk4 / #Gigabyte#

[MeasureFreeDisk4]
Measure=FreeDiskSpace
Drive=#Disk4#

[MeasureFreeDisk4Display]
Measure=Calc
Formula=MeasureFreeDisk4 / #Gigabyte#

[MeasureUsedDisk4]
Measure=FreeDiskSpace
Drive=#Disk4#
InvertMeasure=1

[MeasureUsedDisk4Display]
Measure=Calc
Formula=MeasureUsedDisk4 / #Gigabyte#

; Meters - Disk 4

[MeterBackground]
Meter=Shape
Shape=Rectangle 0,0,#DefaultBoxWidth#,#BoxHeight# | StrokeWidth 1 | Stroke Color #BorderColor# | Fill Color #BackgroundColor#

[MeterDisk4]
Meter=String
MeterStyle=#DiskStyle#
StringAlign=LeftBottom
Text=#Disk4#
X=#HorizontalPadding#r
Y=#NextDiskY#

[MeterDiskFree4]
Meter=String
MeterStyle=#DiskFreeStyle#
StringAlign=LeftBottom
MeasureName=MeasureFreeDisk4Display
NumOfDecimals=1
X=#DiskFreeX#
Y=#DiskFreeY#

[MeterDiskFreeGb4]
Meter=String
MeterStyle=#DiskFreeGbStyle#
StringAlign=LeftBottom
Text=GB
X=#DiskFreeGbX#
Y=#DiskFreeGbY#

[MeterDiskUsed4]
Meter=String
MeterStyle=#DiskUsedStyle#
StringAlign=RightBottom
MeasureName=MeasureUsedDisk4Display
NumOfDecimals=1
Text=%1GB Used
X=#DiskUsedX#
Y=#DiskUsedY#

[MeterFreeBar4]
Meter=Bar
MeasureName=MeasureUsedDisk4
BarOrientation=Horizontal
BarColor=#AccentColor#
SolidColor=#MutedColor#
X=#FreeBarX#
Y=#FreeBarY#
W=#FreeBarW#
H=#FreeBarH#