; Measures - Disk 2

[MeasureTotalDisk2]
Measure=FreeDiskSpace
Drive=#Disk2#
Total=1

[MeasureTotalDisk2Display]
Measure=Calc
Formula=MeasureTotalDisk2 / #Gigabyte#

[MeasureFreeDisk2]
Measure=FreeDiskSpace
Drive=#Disk2#

[MeasureFreeDisk2Display]
Measure=Calc
Formula=MeasureFreeDisk2 / #Gigabyte#

[MeasureUsedDisk2]
Measure=FreeDiskSpace
Drive=#Disk2#
InvertMeasure=1

[MeasureUsedDisk2Display]
Measure=Calc
Formula=MeasureUsedDisk2 / #Gigabyte#

; Meters - Disk 2

[MeterBackground]
Meter=Shape
Shape=Rectangle 0,0,#DefaultBoxWidth#,#BoxHeight# | StrokeWidth 1 | Stroke Color #BorderColor# | Fill Color #BackgroundColor#

[MeterDisk2]
Meter=String
MeterStyle=#DiskStyle#
StringAlign=LeftBottom
Text=#Disk2#
X=#HorizontalPadding#r
Y=#NextDiskY#

[MeterDiskFree2]
Meter=String
MeterStyle=#DiskFreeStyle#
StringAlign=LeftBottom
MeasureName=MeasureFreeDisk2Display
NumOfDecimals=1
X=#DiskFreeX#
Y=#DiskFreeY#

[MeterDiskFreeGb2]
Meter=String
MeterStyle=#DiskFreeGbStyle#
StringAlign=LeftBottom
Text=GB
X=#DiskFreeGbX#
Y=#DiskFreeGbY#

[MeterDiskUsed2]
Meter=String
MeterStyle=#DiskUsedStyle#
StringAlign=RightBottom
MeasureName=MeasureUsedDisk2Display
NumOfDecimals=1
Text=%1GB Used
X=#DiskUsedX#
Y=#DiskUsedY#

[MeterFreeBar2]
Meter=Bar
MeasureName=MeasureUsedDisk2
BarOrientation=Horizontal
BarColor=#AccentColor#
SolidColor=#MutedColor#
X=#FreeBarX#
Y=#FreeBarY#
W=#FreeBarW#
H=#FreeBarH#