[MeasureNews]
Measure=Plugin
Plugin=PluginRss
PageSize=3
Debug=1
@Include1=Feeds.inc
OnUpdateAction=[!CommandMeasure MeasureNewsUpdater "Execute 1"]

[MeasureNewsUpdater]
Measure=Plugin
Plugin=ActionTimer
ActionList1=UpdateChannelTitle | UpdateEntry1Title | UpdateEntry2Title | UpdateEntry3Title | UpdateEntry4Title | UpdateEntry5Title | UpdatePageInfo | RedrawAll
UpdateChannelTitle=[!SetOption "MeterChannelTitle" "Text" [MeasureNews:GetCurrentChannelTitle()]][!UpdateMeter "MeterChannelTitle"]
UpdateEntry1Title=[!SetOption "MeterEntry1Title" "Text" [MeasureNews:GetEntryTitle(0)]][!UpdateMeter "MeterEntry1Title"]
UpdateEntry2Title=[!SetOption "MeterEntry2Title" "Text" [MeasureNews:GetEntryTitle(1)]][!UpdateMeter "MeterEntry2Title"]
UpdateEntry3Title=[!SetOption "MeterEntry3Title" "Text" [MeasureNews:GetEntryTitle(2)]][!UpdateMeter "MeterEntry3Title"]
UpdatePageInfo=[!SetOption "MeterPageInfo" "Text" [MeasureNews:GetPageInfo()]][!UpdateMeter "MeterPageInfo"]
RedrawAll=[!Redraw]

[MeterBackground]
Meter=Shape
Shape=Rectangle 0,0,#DefaultBoxWidth#,#BoxHeight# | StrokeWidth 1 | Stroke Color #BorderColor# | Fill Color #BackgroundColor#

[MeterPreviousChannel]
Meter=Shape
X=#LeftPadding#
Y=#TopIconY#
Shape=Path PreviousIcon | StrokeWidth 0 | Extend IconColor
PreviousIcon=0, (#ShapeHeight# * 0.5) | LineTo #ShapeWidth#, 0 | LineTo #ShapeWidth#, #ShapeHeight# | ClosePath 1
IconColor=FillColor #AccentColor#
MouseActionCursor=1
LeftMouseUpAction=[!CommandMeasure "MeasureNews" "PreviousChannel"][!CommandMeasure "MeasureNewsUpdater" "Execute 1"]

[MeterNextChannel]
Meter=Shape
X=(#DefaultBoxWidth#-#LeftPadding#-#ShapeWidth#)
Y=#TopIconY#
Shape=Path NextIcon | StrokeWidth 0 | Extend IconColor
NextIcon=#ShapeWidth#, (#ShapeHeight# * 0.5) | LineTo 0, 0 | LineTo 0, #ShapeHeight# | ClosePath 1
IconColor=FillColor #AccentColor#
MouseActionCursor=1
LeftMouseUpAction=[!CommandMeasure "MeasureNews" "NextChannel"][!CommandMeasure "MeasureNewsUpdater" "Execute 1"]

[MeterChannelTitle]
Meter=String
MeterStyle=StyleSecondaryText
StringAlign=Center
X=(#DefaultBoxWidth# / 2)
Y=#TopPadding#
W=(#DefaultBoxWidth# - 4 * #LeftPadding# - 2 * #ShapeWidth#)
ClipString=1

[MeterEntry1Title]
Meter=String
MeterStyle=StyleSmallText
X=#LeftPadding#
Y=5R
W=(#DefaultBoxWidth# - 2 * #LeftPadding#)
H=(#SmallTextHeight# * 2)
ClipString=2
MouseActionCursor=1
LeftMouseUpAction=[!Execute [MeasureNews:GetEntryLink(0)]]

[MeterEntry2Title]
Meter=String
MeterStyle=StyleSmallText
X=#LeftPadding#
Y=5R
W=(#DefaultBoxWidth# - 2 * #LeftPadding#)
H=(#SmallTextHeight# * 2)
ClipString=2
MouseActionCursor=1
LeftMouseUpAction=[!Execute [MeasureNews:GetEntryLink(1)]]

[MeterEntry3Title]
Meter=String
MeterStyle=StyleSmallText
X=#LeftPadding#
Y=5R
W=(#DefaultBoxWidth# - 2 * #LeftPadding#)
H=(#SmallTextHeight# * 2)
ClipString=2
MouseActionCursor=1
LeftMouseUpAction=[!Execute [MeasureNews:GetEntryLink(2)]]

[MeterPreviousPage]
Meter=String
MeterStyle=StyleSmallText
X=#LeftPadding#
Y=5R
StringAlign=Left
Text=< Previous page
MouseActionCursor=1
SolidColor=0,0,0,1
FontColor=#AccentColor#
LeftMouseUpAction=[!CommandMeasure "MeasureNews" "PreviousPage"][!CommandMeasure "MeasureNewsUpdater" "Execute 1"]

[MeterNextPage]
Meter=String
MeterStyle=StyleSmallText
X=(#DefaultBoxWidth# - #LeftPadding#)
Y=0r
StringAlign=Right
Text=Next page >
MouseActionCursor=1
SolidColor=0,0,0,1
FontColor=#AccentColor#
LeftMouseUpAction=[!CommandMeasure "MeasureNews" "NextPage"][!CommandMeasure "MeasureNewsUpdater" "Execute 1"]

[MeterPageInfo]
Meter=String
MeterStyle=StyleSmallText
X=(#DefaultBoxWidth# / 2)
Y=0r
StringAlign=Center
Text=0 / 0
FontColor=#AccentColor#