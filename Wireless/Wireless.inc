; Measures

[MeasureWifiSSID]
Measure=Plugin
Plugin=WiFiStatus
WiFiInfoType=SSID

[MeasureWiFiPower]
Measure=Plugin
Plugin=WiFiStatus
WiFiInfoType=Quality
IfCondition=(MeasureWiFiPower >= 75)
IfTrueAction=[!SetOption MeterIconArc3 FillColor "Fill Color #EmphasizeColor#"][!SetOption MeterIconArc2 FillColor "Fill Color #EmphasizeColor#"][!SetOption MeterIconArc1 FillColor "Fill Color #EmphasizeColor#"]
IfCondition2=(MeasureWiFiPower >= 50) && (MeasureWiFiPower < 75)
IfTrueAction2=[!SetOption MeterIconArc3 FillColor "Fill Color #MutedColor#"][!SetOption MeterIconArc2 FillColor "Fill Color #EmphasizeColor#"][!SetOption MeterIconArc1 FillColor "Fill Color #EmphasizeColor#"]
IfCondition3=(MeasureWiFiPower >= 25) && (MeasureWiFiPower < 50)
IfTrueAction3=[!SetOption MeterIconArc3 FillColor "Fill Color #MutedColor#"][!SetOption MeterIconArc2 FillColor "Fill Color #MutedColor#"][!SetOption MeterIconArc1 FillColor "Fill Color #EmphasizeColor#"]
IfCondition4=(MeasureWiFiPower < 25)
IfTrueAction4=[!SetOption MeterIconArc3 FillColor "Fill Color #MutedColor#"][!SetOption MeterIconArc2 FillColor "Fill Color #MutedColor#"][!SetOption MeterIconArc1 FillColor "Fill Color #MutedColor#"]

[MeasureWiFiAuth]
Measure=Plugin
Plugin=WiFiStatus
WiFiInfoType=Auth

[MeasureWiFiEncryption]
Measure=Plugin
Plugin=WiFiStatus
WiFiInfoType=Encryption

; Meters

[MeterBackground]
Meter=Shape
Shape=Rectangle 0,0,#DefaultBoxWidth#,#BoxHeight# | StrokeWidth 1 | Stroke Color #BorderColor# | Fill Color #BackgroundColor#

[MeterIconDot]
Meter=Shape
X=(#WiFiIconX#)
Y=(#WiFiIconY#+#WiFiIconH#)
Shape=Ellipse (#WiFiBarWidth#),(-#WiFiBarWidth#),(#WiFiBarWidth#),(#WiFiBarWidth#) | Extend FillColor
FillColor=Fill Color #EmphasizeColor#

[MeterIconArc1]
Meter=Shape
X=(#WiFiIconX#)
Y=(#WiFiIconY#+#WiFiIconH#)
Shape=Path Arc1 | Extend FillColor
Arc1=(#WiFiArc1R2#),0 | CurveTo 0,(-#WiFiArc1R2#),(#WiFiArc1R2#),(-#WiFiArc1R2#) | LineTo 0,(-#WiFiArc1R1#) | CurveTo (#WiFiArc1R1#),0,(#WiFiArc1R1#),(-#WiFiArc1R1#) | ClosePath 1
FillColor=Fill Color #MutedColor#

[MeterIconArc2]
Meter=Shape
X=(#WiFiIconX#)
Y=(#WiFiIconY#+#WiFiIconH#)
Shape=Path Arc2 | Extend FillColor
Arc2=(#WiFiArc2R2#),0 | CurveTo 0,(-#WiFiArc2R2#),(#WiFiArc2R2#),(-#WiFiArc2R2#) | LineTo 0,(-#WiFiArc2R1#) | CurveTo (#WiFiArc2R1#),0,(#WiFiArc2R1#),(-#WiFiArc2R1#) | ClosePath 1
FillColor=Fill Color #MutedColor#

[MeterIconArc3]
Meter=Shape
X=(#WiFiIconX#)
Y=(#WiFiIconY#+#WiFiIconH#)
Shape=Path Arc3 | Extend FillColor
Arc3=(#WiFiArc3R2#),0 | CurveTo 0,(-#WiFiArc3R2#),(#WiFiArc3R2#),(-#WiFiArc3R2#) | LineTo 0,(-#WiFiArc3R1#) | CurveTo (#WiFiArc3R1#),0,(#WiFiArc3R1#),(-#WiFiArc3R1#) | ClosePath 1
FillColor=Fill Color #MutedColor#

[MeterWifiSSID]
Meter=String
MeterStyle=#WifiSSIDStyle#
MeasureName=MeasureWifiSSID
StringAlign=LeftCenter
ClipString=1
X=#WiFiSSIDX#
Y=#WiFiSSIDY#
W=(#DefaultBoxWidth#-#WiFiSSIDX#-#LeftPadding#)

[MeterWiFiData]
Meter=String
MeterStyle=#WiFiDataStyle#
MeasureName=MeasureWiFiAuth
MeasureName2=MeasureWiFiEncryption
Text=%1 %2
StringAlign=LeftTop
X=#WiFiDataX#
Y=#WiFiDataY#