[MeasureBatteryPercentage]
Measure=Plugin
Plugin=PowerPlugin
PowerState=Percent

[MeasureBatteryLifetime]
Measure=Plugin
Plugin=PowerPlugin
PowerState=Lifetime

[MeasureBatteryACLine]
Measure=Plugin
Plugin=PowerPlugin
PowerState=ACLine
IfCondition=MeasureBatteryACLine = 0
IfTrueAction=[!ShowMeter MeterLifetime][!HideMeter MeterAC][!SetOption MeterFreeBar BarColor #EmphasizeColor#][!SetOption MeterBatteryIcon ShapeColor "FillColor #EmphasizeColor#"]
IfFalseAction=[!HideMeter MeterLifetime][!ShowMeter MeterAC][!SetOption MeterFreeBar BarColor #MutedColor#][!SetOption MeterBatteryIcon ShapeColor "FillColor #MutedColor#"]
OnUpdateAction=[!UpdateMeter MeterLifetime][!UpdateMeter MeterFreeBar][!Redraw]

; Meters

[MeterBackground]
Meter=Shape
Shape=Rectangle 0,0,#DefaultBoxWidth#,#BoxHeight# | StrokeWidth 1 | Stroke Color #BorderColor# | Fill Color #BackgroundColor#

[MeterBatteryIcon]
Meter=Shape
X=#BatteryIconX#
Y=#BatteryIconY#
Shape=Rectangle 0,0,#BatteryBorderWidth#,#BatteryHeight# | StrokeWidth 0 | Extend ShapeColor
Shape2=Rectangle (0+#BatteryWidth#-#BatteryBorderWidth#),0,#BatteryBorderWidth#,#BatteryHeight# | StrokeWidth 0  | Extend ShapeColor
Shape3=Rectangle 0,0,#BatteryWidth#,#BatteryBorderWidth# | StrokeWidth 0  | Extend ShapeColor
Shape4=Rectangle 0,(0+#BatteryHeight#-#BatteryBorderWidth#),#BatteryWidth#,#BatteryBorderWidth# | StrokeWidth 0  | Extend ShapeColor
Shape5=Rectangle (0 + #BatteryWidth# - #BatteryBorderWidth#),(0+(1/3)*#BatteryHeight#),(2*#BatteryBorderWidth#),((1/3)*#BatteryHeight#)| StrokeWidth 0  | Extend ShapeColor
ShapeColor=FillColor #EmphasizeColor#

[MeterFreeBar]
Meter=Bar
MeasureName=MeasureBatteryPercentage
BarOrientation=Horizontal
BarColor=#EmphasizeColor#
SolidColor=0,0,0,0
X=#BatteryBarX#
Y=#BatteryBarY#
W=#BatteryBarW#
H=#BatteryBarH#

[MeterPercentage]
Meter=String
MeterStyle=#BatteryPercentStyle#
MeasureName=MeasureBatteryPercentage
StringAlign=LeftCenter
Text=%1%
X=#BatteryPercentX#
Y=#BatteryPercentY#

[MeterLifetime]
Meter=String
MeterStyle=#BatteryLifetimeStyle#
MeasureName=MeasureBatteryLifetime
StringAlign=RightCenter
Text=%1
X=#BatteryLifetimeX#
Y=#BatteryLifetimeY#

[MeterAC]
Meter=String
MeterStyle=#BatteryLifetimeStyle#
StringAlign=RightCenter
Text=AC powered
X=#BatteryLifetimeX#
Y=#BatteryLifetimeY#