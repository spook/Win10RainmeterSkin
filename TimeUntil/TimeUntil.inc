[MeasureCurrentDateTime]
Measure=Time
;Format=%Y-%m-%dT%H:%M:%S

[MeasureTargetDate]
Measure=Time
TimeStamp=2023-10-29T00:00:00
TimeStampFormat=%Y-%m-%dT%H:%M:%S

[MeasureDaysDifference]
Measure=Calc
Formula=(TRUNC(ABS([MeasureTargetDate:TimeStamp] - [MeasureCurrentDateTime:TimeStamp]) / 86400))
DynamicVariables=1

[ConvertedTimeDelta]
Measure=Time
TimeStamp=[MeasureDaysDifference:]
DynamicVariables=1

[MeterBackground]
Meter=Shape
Shape=Rectangle 0,0,#DefaultBoxWidth#,#BoxHeight# | StrokeWidth 1 | Stroke Color #BorderColor# | Fill Color #BackgroundColor#

[MeterTitle]
Meter=String
ClipString=1
Text=Days until Kodiaq:
MeterStyle=#TitleStyle#
X=#TitleX#
Y=#TitleY#

[MeterTimeDiff]
Meter=String
ClipString=1
MeasureName=MeasureDaysDifference
MeterStyle=#ValueStyle#
X=5R
Y=#TitleY#

