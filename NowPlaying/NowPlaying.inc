[MeasurePlayer]
Measure=NowPlaying
PlayerName=#PlayerName#
PlayerType=Artist

[MeasureArtist]
Measure=NowPlaying
PlayerName=#PlayerName#
PlayerType=Artist

[MeasureTitle]
Measure=NowPlaying
PlayerName=#PlayerName#
PlayerType=Title

[MeterBackground]
Meter=Shape
Shape=Rectangle 0,0,#DefaultBoxWidth#,#BoxHeight# | StrokeWidth 1 | Stroke Color #BorderColor# | Fill Color #BackgroundColor#

[MeterTitle]
Meter=String
ClipString=1
MeasureName=MeasureTitle
MeterStyle=#TitleStyle#
X=#TitleX#
Y=#TitleY#
W=#TitleW#

[MeterArtist]
Meter=String
ClipString=1
MeasureName=MeasureArtist
MeterStyle=#ArtistStyle#
X=#ArtistX#
Y=#ArtistY#
W=#ArtistW#

[MeterPlayPause]
Meter=Shape
X=#PlayPauseX#
Y=#PlayPauseY#
Shape=Path PlayIcon | StrokeWidth 0 | Extend IconColor
Shape2=Rectangle #ShapeSize# * (0.5 + #RectangleSpacing#), (-#ShapeSize# * 0.5), (#ShapeSize# * #RectangleRatio#), (#ShapeSize#) | StrokeWidth 0 | Extend IconColor
Shape3=Rectangle #ShapeSize# * (0.5 + #RectangleRatio# + 2 * #RectangleSpacing#), (-#ShapeSize# * 0.5), (#ShapeSize# * #RectangleRatio#), (#ShapeSize#) | StrokeWidth 0 | Extend IconColor
PlayIcon=MoveTo 0,0 | LineTo 0, (-#ShapeSize# * 0.5) | LineTo (#ShapeSize# * 0.5), 0 | LineTo 0, (#ShapeSize# * 0.5) | ClosePath 1
IconColor=FillColor #MutedColor#
MouseOverAction=[!SetOption MeterPlayPause IconColor "FillColor #EmphasizeColor#"]
MouseLeaveAction=[!SetOption MeterPlayPause IconColor "FillColor #MutedColor#"]
MouseActionCursor=1
LeftMouseUpAction=[!CommandMeasure "MeasurePlayer" "PlayPause"]

[MeterNext]
Meter=Shape
X=#NextX#
Y=#NextY#
Shape=Path NextIcon | StrokeWidth 0 | Extend IconColor
Shape2=Rectangle #ShapeSize# * (0.5 + #RectangleSpacing#), (-#ShapeSize# * 0.5), (#ShapeSize# * #RectangleRatio#), (#ShapeSize#) | StrokeWidth 0 | Extend IconColor
NextIcon=0, (-#ShapeSize# * 0.5) | LineTo (#ShapeSize# * 0.5), 0 | LineTo 0, (#ShapeSize# * 0.5) | ClosePath 1
IconColor=FillColor #MutedColor#
MouseOverAction=[!SetOption MeterNext IconColor "FillColor #EmphasizeColor#"]
MouseLeaveAction=[!SetOption MeterNext IconColor "FillColor #MutedColor#"]
MouseActionCursor=1
LeftMouseUpAction=[!CommandMeasure "MeasurePlayer" "Next"]

[MeterPrevious]
Meter=Shape
X=#PreviousX#
Y=#PreviousY#
Shape=Rectangle 0, (-#ShapeSize# * 0.5), (#ShapeSize# * #RectangleRatio#), (#ShapeSize#) | StrokeWidth 0 | Extend IconColor
Shape2=Path PrevIcon | StrokeWidth 0 | Extend IconColor
PrevIcon=(#ShapeSize# * (#RectangleRatio# + #RectangleSpacing#)),0 | LineTo (#ShapeSize# * (0.5 + #RectangleRatio# + #RectangleSpacing#)), (-#ShapeSize# * 0.5) | LineTo (#ShapeSize# * (0.5 + #RectangleRatio# + #RectangleSpacing#)), (#ShapeSize# * 0.5) | LineTo (#ShapeSize# * (#RectangleRatio# + #RectangleSpacing#)), 0 | ClosePath 1
IconColor=FillColor #MutedColor#
MouseOverAction=[!SetOption MeterPrevious IconColor "FillColor #EmphasizeColor#"]
MouseLeaveAction=[!SetOption MeterPrevious IconColor "FillColor #MutedColor#"]
MouseActionCursor=1
LeftMouseUpAction=[!CommandMeasure "MeasurePlayer" "Previous"]